#! /bin/bash
# Wrapper to run Dedalus.py with the correct python interpreter
source ${DEDALUS_HOME}/bin/activate
DEX_HOME=${DEDALUS_HOME}/lib/python3.6/site-packages/dedalus_extensions
if [ ! -z $@ ]
then
	dedalus ${DEX_HOME}/Main.py "$@"
fi
