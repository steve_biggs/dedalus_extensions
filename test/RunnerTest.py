"""
Unit tests of Runner

Copyright 2018 Stephen Biggs-Fox

This file is part of dedalusextensions.

dedalusextensions is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

dedalusextensions is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with dedalusextensions.  If not, see http://www.gnu.org/licenses/.
"""

from unittest import TestCase
import sys
sys.path.insert(0, '../src')
from Runner import Runner


class RunnerTest(TestCase):

    def setUp(self):
        self.runConfigFilePath = './run.cfg'
        self.runner = Runner('FakeModel', '.', self.runConfigFilePath)

    def testRunnerImportsClassFromModelFromGivenDirectory(self):
        self.runner.run()
        self.assertTrue(self.runner.model.wasInstantiated)

    def testRunnerGetsModelToReadConfigFile(self):
        self.runner.run()
        self.assertTrue(self.runner.model.wasConfigRead)

    def testRunnerGetsModelToReadConfigFileWithCorrectFilePath(self):
        self.runner.run()
        self.assertEqual(self.runner.model.runConfigFilePath, self.runConfigFilePath)

    def testRunnerSetsUpModel(self):
        self.runner.run()
        self.assertTrue(self.runner.model.wasSetUp)

    def testRunnerSolvesModel(self):
        self.runner.run()
        self.assertTrue(self.runner.model.wasSolved)

